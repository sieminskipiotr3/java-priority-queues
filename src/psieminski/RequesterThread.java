package psieminski;

import java.util.NoSuchElementException;

public class RequesterThread extends Thread {

    private final CalculatingService service;
    private final Task task;
    private Integer result;

    public RequesterThread(CalculatingService service, Task task) {
        this.service = service;
        this.task = task;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public void run() {
        service.addTask(this.task);
        do {
            try {
                service.getResults()
                        .stream()
                        .filter(r -> r.getTask().equals(this.task))
                        .map(Result::getResult)
                        .forEach(this::setResult);
            } catch (NoSuchElementException ignored) {
            }
        } while (!service.getRequests().isEmpty());
    }

    public Integer getResult() {
        if (this.result == null) {
            throw new IllegalStateException("Result has not been yet calculated");
        }
        return result;
    }
}
