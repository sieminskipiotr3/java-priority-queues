package psieminski;

import java.util.NoSuchElementException;

public class Result implements Comparable<Result> {

    private final Task task;
    private Integer result;

    public Result(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }

    public Integer getResult() {
        if (result == null) {
            throw new NoSuchElementException("Task has no solution yet");
        }
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Integer getTaskPriorityLevel() {
        return task.getPriorityLevel();
    }

    @Override
    public int compareTo(Result o) {
        return this.getTaskPriorityLevel().compareTo(o.getTaskPriorityLevel());
    }
}

