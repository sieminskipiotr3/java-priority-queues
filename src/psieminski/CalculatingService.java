package psieminski;

import java.util.concurrent.PriorityBlockingQueue;

public class CalculatingService {

    private final PriorityBlockingQueue<Task> requests;
    private final PriorityBlockingQueue<Result> results;

    public CalculatingService() {
        this.requests = new PriorityBlockingQueue<>(20, Task::compareTo);
        this.results = new PriorityBlockingQueue<>(20, Result::compareTo);
    }

    public PriorityBlockingQueue<Task> getRequests() {
        return requests;
    }

    public PriorityBlockingQueue<Result> getResults() {
        return results;
    }

    public void addTask(Task task) {
        synchronized (this) {
            requests.add(task);
            notifyAll();
        }
    }

    public synchronized void calculateRequest() throws InterruptedException {
        try {
            Result currentResult = new Result(requests.take());
            currentResult.setResult(currentResult.getTask().calculateSum());
            results.add(currentResult);
            notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
