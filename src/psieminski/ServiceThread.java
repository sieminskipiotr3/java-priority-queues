package psieminski;

public class ServiceThread extends Thread {

    private final CalculatingService service;

    public ServiceThread(CalculatingService service) {
        this.service = service;
    }

    public void run() {
        try {
            service.calculateRequest();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
