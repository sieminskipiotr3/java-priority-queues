package psieminski.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import psieminski.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThreadTest {


    CalculatingService service = new CalculatingService();
    Task task1 = new Task(1, 2, "LOW");
    Task task2 = new Task(3, 4, "HIGH");
    Task task3 = new Task(5, 6, "LOW");
    Task task4 = new Task(7, 8, "NORMAL");
    Task task5 = new Task(9, 10, "LOW");
    Task task6 = new Task(11, 12, "LOW");

    RequesterThread thread1 = new RequesterThread(service, task1);
    RequesterThread thread2 = new RequesterThread(service, task2);
    RequesterThread thread3 = new RequesterThread(service, task3);
    RequesterThread thread4 = new RequesterThread(service, task4);
    RequesterThread thread5 = new RequesterThread(service, task5);
    RequesterThread thread6 = new RequesterThread(service, task6);

    ServiceThread serviceThread1 = new ServiceThread(service);

    Integer actual;

    @BeforeEach
    void init() {

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();

        serviceThread1.start();
    }

    @Test
    public void priorityTest() {

        Integer expected = 7;
        actual = thread2.getResult();

        assertEquals(expected, actual);

    }

    @Test
    public void concurrencyTest() {
        List<Integer> checkTaskQueue = service.getRequests()
                .stream()
                .map(Task::getFirstNumber)
                .collect(Collectors.toList());

        Integer expected = 7;
        Integer actual = checkTaskQueue.get(0);

        assertEquals(expected, actual);

    }

}
