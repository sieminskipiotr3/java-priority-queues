package psieminski.tests;

import org.junit.jupiter.api.BeforeEach;
import psieminski.CalculatingService;
import psieminski.Task;

import static org.junit.jupiter.api.Assertions.*;

class CalculatingServiceTest {

    CalculatingService service = new CalculatingService();
    Task task1 = new Task(1, 2, "LOW");
    Task task2 = new Task(3, 4, "HIGH");
    Task task3 = new Task(5, 6, "NORMAL");
    Task task4 = new Task(7, 8, "NORMAL");
    Task task5 = new Task(9, 10, "LOW");
    Task task6 = new Task(11, 12, "LOW");

    @BeforeEach
    void init() {
        service.addTask(task1);
        service.addTask(task2);
        service.addTask(task3);
    }

    @org.junit.jupiter.api.Test
    void addTask() throws InterruptedException {

        service.addTask(task4);
        service.addTask(task5);
        service.addTask(task6);

        Integer expected = 3;
        Integer actual = service.getRequests().take().getFirstNumber();

        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void calculateRequest() throws InterruptedException {

        service.calculateRequest();
        service.calculateRequest();
        service.calculateRequest();

        Integer expected = 3;
        Integer actual = service.getResults().take().getTask().getFirstNumber();

        assertEquals(expected, actual);

    }
}