package psieminski;

import java.util.Objects;

public class Task implements Comparable<Task> {

    private final Integer firstNumber;
    private final Integer secondNumber;
    private final Integer priorityLevel;

    public Task(Integer firstNumber, Integer secondNumber, String priorityLevel) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        switch (priorityLevel) {
            case "HIGH" -> this.priorityLevel = 1;
            case "NORMAL" -> this.priorityLevel = 2;
            default -> this.priorityLevel = 3;
        }
    }

    public Integer getFirstNumber() {
        return firstNumber;
    }

    public Integer getSecondNumber() {
        return secondNumber;
    }

    public Integer getPriorityLevel() {
        return priorityLevel;
    }

    public Integer calculateSum() {
        return firstNumber + secondNumber;
    }

    @Override
    public int compareTo(Task o) {
        return this.getPriorityLevel().compareTo(o.getPriorityLevel());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return firstNumber.equals(task.firstNumber) && secondNumber.equals(task.secondNumber) && priorityLevel.equals(task.priorityLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstNumber, secondNumber, priorityLevel);
    }

}

